﻿create table Game(
GameID INT PRIMARY KEY IDENTITY NOT NULL,
ForeignUserID INT FOREIGN KEY REFERENCES TuskeUser(UserID) NOT NULL,
Steps INT NOT NULL,
Time TIME NOT NULL,
Result VARCHAR(10) NOT NULL CHECK (Result IN('Win','Defeat')),
StartDate DateTime NOT NULL,
EndDate DateTime NOT NULL);