﻿create table Profile(
ProfileID INT PRIMARY KEY IDENTITY NOT NULL,
ForeignUserID INT FOREIGN KEY REFERENCES TuskeUser(UserID) NOT NULL,
UserName VARCHAR(255) NOT NULL,
BirthDate DATE NOT NULL,
Gender VARCHAR(10) NOT NULL CHECK (Gender IN('Male','Female')),
ModifiedDate DateTime NOT NULL);

INSERT INTO Profile (ForeignUserID, UserName, BirthDate, Gender, ModifiedDate) VALUES 
(1, 'Teszt Béla', '1980-01-01', 'Male', GETDATE());