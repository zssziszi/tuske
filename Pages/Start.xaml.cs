﻿using System.Windows;
using System.Windows.Controls;


namespace Tuske
{
    public partial class Start : Page
    {
        public string UserName { get; set; }
        public Start()
        {
            UserName = UserSession.user.UserName;
            InitializeComponent();
            this.DataContext = this;
        }

        private void BtnStartToPlayClick(object sender, RoutedEventArgs e)
        {
            MainWindow.OpenGamePage(null, null, null);
        }

        private void BtnStatiscticsClick(object sender, RoutedEventArgs e)
        {
            MainWindow.ChangePageSource("Pages/Statistics.xaml");
        }

        private void BtnArchiveClick(object sender, RoutedEventArgs e)
        {
            MainWindow.ChangePageSource("Pages/Archive.xaml");
        }
    }
}
