﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Windows;
using System.Windows.Controls;
using System.Data;
using System.Data.SqlClient;


namespace Tuske
{
    public partial class Archive : Page
    {
        public Archive()
        {
            
            InitializeComponent();
            DgvUpload();
        }

        private void DgvUpload()
        {
            
            MainWindow.conn.Open();
            string cmdString = $"SELECT StartDate, Steps, Time, Result, GameID FROM Game WHERE ForeignUserID = {UserSession.user.Id} ORDER BY GameID DESC";

            SqlCommand cmd = new SqlCommand(cmdString, MainWindow.conn);

            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            
            DataTable dt = new DataTable("Game");

            sda.Fill(dt);

            dgvArchive.ItemsSource = dt.DefaultView;

            MainWindow.conn.Close();
        }

        private void BtnBackClick(object sender, RoutedEventArgs e)
        {
            MainWindow.ChangePageSource("Pages/Start.xaml");
        }

        private void DgvArchiveSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataRowView dataRowView = (DataRowView)dgvArchive.SelectedItem;
            int gameID = Convert.ToInt32(dataRowView.Row[4]);
            string gameTime = Convert.ToString(dataRowView.Row[2]);
            SelectArchive(gameID,gameTime);
        }

        private void SelectArchive(int gameID, string gameTime)
        {
            MainWindow.conn.Open();
            string query = $"SELECT * FROM Archive WHERE ForeignGameID = {gameID}";
            var r = new SqlCommand(query, MainWindow.conn).ExecuteReader();

            if (r.Read())
            {
                int[] hiddenCodeIndexes = r[2].ToString().Split(',').Select(Int32.Parse).ToArray();
                List<Color> hiddenCode = new List<Color>();

                for (int i = 0; i < hiddenCodeIndexes.Length; i++)
                {
                    hiddenCode.Add(Config.GetCodePegColorByIndex(hiddenCodeIndexes[i]));
                }

                List<GuessAndFeedback> gameData = JsonSerializer.Deserialize<List<GuessAndFeedback>>(r[3].ToString());

                MainWindow.OpenGamePage(hiddenCode, gameData, gameTime);
            }
            MainWindow.conn.Close();
        }
    }
}
