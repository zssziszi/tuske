﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Diagnostics;
using System.Windows.Threading;

namespace Tuske
{
    [Serializable]
    public struct GuessAndFeedback
    {
        public int[] guess { get; set; }
        public int correctColor { get; set; }
        public int correctPosition { get; set; }
        public bool clickable { get; set; }
    }

    public partial class Game : Page
    {
        private static Random rnd;
        public static List<Color> hiddenCode;
        private static List<Color> colorListToHide; //clone of codePegColors list (ebből képezzük random a hiddenCode színeit. Azért kell ez a klón, hogy remove AT-tel kivehessük a már kiforgatott színt, hogy ne legyen színduplikáció.)
        public static List<GuessAndFeedback> gameData;
        public static DispatcherTimer dispatcherTimer;
        public static Stopwatch stopWatch;
        public static string currentTime;
        public static DateTime startTime;

        public static Game Instance { get; private set; }
        public static bool IsArchive {get; set;}
        public static string KebabVisibility {get; set;}
        public static string XButtonVisibility { get; set; }

        public Game(List<Color> hiddenCodeParameter, List<GuessAndFeedback> gameDataParameter, string gameTime)
        {
            rnd = new Random();
            IsArchive = false;
            KebabVisibility = "Visible";
            XButtonVisibility = "Hidden";
            colorListToHide = new List<Color>(Config.codePegColors);
            dispatcherTimer = new DispatcherTimer();
            stopWatch = new Stopwatch();
            currentTime = string.Empty;
            startTime = DateTime.Now;

            if (hiddenCodeParameter == null)
            {
                hiddenCode = new List<Color>();
                for (int i = 0; i < 4; i++)
                {
                    int randomIndex = rnd.Next(colorListToHide.Count - 1);
                    hiddenCode.Add(colorListToHide[randomIndex]);
                    colorListToHide.RemoveAt(randomIndex);
                }
            }
            else
            {
                hiddenCode = hiddenCodeParameter;
                IsArchive = true;
                KebabVisibility = "Hidden";
                XButtonVisibility = "Visible";
            }

            if (gameDataParameter == null)
            {
                gameData = new List<GuessAndFeedback>();
            }
            else gameData = gameDataParameter;

            InitializeComponent();

            this.DataContext = this;

            Instance = this;

            GamePlayGrid.Children.Add(new GamePlay());
            HeaderGrid.Children.Add(new HiddenCode());

            if (gameTime == null)
            {
                dispatcherTimer.Tick += new EventHandler(DtTick);
                dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1);
                stopWatch.Start();
                dispatcherTimer.Start();
            }
            else
            {
                lblTime.Content = Config.CustomTimeFormat(gameTime);
            }

        }

        public static void Assessment(int[] guess)
        {
            int correctColor = 0;
            int correctPosition = 0;

            for (int i = 0; i < guess.Length; i++)
            {    //correct position assessment
                if (guess[i] == Config.GetCodePegColorByHexa(hiddenCode[i].hexa))
                {
                    correctPosition++;
                }
                 //correct color but not in correct position assessment
                else if (guess.Contains(Config.GetCodePegColorByHexa(hiddenCode[i].hexa))) /*&& tipsToString[i] != hiddenColorCombination[i])*/
                {
                    correctColor++;
                }
            }

            gameData.Add(new GuessAndFeedback()
            {
                guess = guess,
                correctPosition = correctPosition,
                correctColor = correctColor,
            });

            if (correctPosition == 4)
            {
                //GameEndScreen - WIN
                Instance.GameGrid.Children.Add(new GameEndScreen(true));
                stopWatch.Stop();
                dispatcherTimer.Stop();
            }

            else if (gameData.Count >= 10)
            {
                //GameEndScreen - DEFEAT
                Instance.GameGrid.Children.Add(new GameEndScreen(false));
            }

            else RedrawGamePlay();
        }

        public static void RedrawGamePlay()
        {
            Instance.GamePlayGrid.Children.Clear();
            Instance.GamePlayGrid.Children.Add(new GamePlay());
        }

        public static void RemoveChild(UserControl Child)
        {
            Instance.GameGrid.Children.Remove(Child);
        }

        public void DtTick(object sender, EventArgs e)
        {
            if (stopWatch.IsRunning)
            {
                TimeSpan ts = stopWatch.Elapsed;
                currentTime = String.Format("{0:00}:{1:00}.{2:00}", ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                lblTime.Content = currentTime;
            }
        }

        private void BtnMenuClick(object sender, RoutedEventArgs e)
        {
            Instance.GameGrid.Children.Add(new PauseMenu());
        }

        private void BtnXClick(object sender, RoutedEventArgs e)
        {
            MainWindow.ChangePageSource("Pages/Archive.xaml");
        }
    }
}
