﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Data.SqlClient;


namespace Tuske
{
    public partial class Login : Page
    {
        public string DefaultEmail { get; set; }

        public Login()
        {
            #if DEBUG
                DefaultEmail = "test@test.hu";
            #else
                DefaultEmail = "";     
            #endif

            InitializeComponent();
            this.DataContext = this;
        }

        private void BtnRegistrationClick(object sender, RoutedEventArgs e)
        {
            MainWindow.ChangePageSource("Pages/Registration.xaml");
        }

        private void DataSelection(string email, string password)
        {
            MainWindow.conn.Open();
            string query = $"SELECT UserID, Email, UserName, BirthDate, Gender FROM TuskeUser LEFT JOIN Profile ON TuskeUser.UserID = Profile.ForeignUserID WHERE Email = '{email}' COLLATE SQL_Latin1_General_CP1_CS_AS AND Password = '{password}'";

            //cs (case sensitive)
           
            var r = new SqlCommand(query, MainWindow.conn).ExecuteReader();

            if (r.Read())
            {
                UserSession.CreateUser(Convert.ToInt32(r[0]), Convert.ToString(r[1]), Convert.ToString(r[2]), Convert.ToDateTime(r[3]), Convert.ToString(r[4]));
                MainWindow.ChangePageSource("Pages/Start.xaml");
            }
            else
            {
                lblRegFailure.Visibility = Visibility.Visible;
            }
            MainWindow.conn.Close();
        }

        private void BtnLoginClick(object sender, RoutedEventArgs e)
        {
            DataSelection(tbEmail.Text, Config.CreateMD5(pbPassword.Password));
        }
    }
}
