﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Data.SqlClient;


namespace Tuske
{
 
    public partial class Registration : Page
    {
        
        public Registration()
        {
            InitializeComponent();
        }

        private void BtnLoginClick(object sender, RoutedEventArgs e)
        {
            MainWindow.ChangePageSource("Pages/Login.xaml");
        }

        private void BtnRegistrationClick(object sender, RoutedEventArgs e)
        {
            if (FormValidation())
            {
                DataUploadtoDatabase();
            }
        }

        private bool FormValidation()
        {
            lblEmailMessage.Visibility = Visibility.Hidden;
            lblPasswordMessage.Visibility = Visibility.Hidden;
            lblUserNameMessage.Visibility = Visibility.Hidden;
            lblBirthdayMessage.Visibility = Visibility.Hidden;
            lblGenderMessage.Visibility = Visibility.Hidden;


            if (!Config.IsValidEmail(tbEmail.Text))
            {
                lblEmailMessage.Visibility = Visibility.Visible;
                return false;
            }

            if (tbPassword.Password.Length < 8)
            {
                lblPasswordMessage.Visibility = Visibility.Visible;
                return false;
            }

            if (tbUserName.Text.Length < 3)
            {
                lblUserNameMessage.Visibility = Visibility.Visible;
                return false;
            }

            if (dpBirthDate.SelectedDate == null || dpBirthDate.SelectedDate > DateTime.Today)
            {
                lblBirthdayMessage.Visibility = Visibility.Visible;
                return false;
            }

            if (rbMale.IsChecked == false && rbFemale.IsChecked == false)
            {
                lblGenderMessage.Visibility = Visibility.Visible;
                return false;
            }

            return true;
        }

        private void DataUploadtoDatabase()
        {
            MainWindow.conn.Open();
            int insertedID = 0;
            int profileInsertedID = 0;
            string email = tbEmail.Text;
            string password = Config.CreateMD5(tbPassword.Password);
            string username = tbUserName.Text;
            string birthdate = dpBirthDate.SelectedDate.Value.Date.ToShortDateString();
            
            string gender;
            if (rbMale.IsChecked == true)
            {
                gender = rbMale.Content.ToString();
            }
            else 
            {
                gender = rbFemale.Content.ToString();
            }
            
            try
            {
                var cmd = new SqlCommand(
                    "INSERT INTO TuskeUser (Email, Password, RegDate, LastLoginDate) VALUES " +
                    $"('{email}', '{password}', GETDATE(), GETDATE()); SELECT SCOPE_IDENTITY();", 
                    MainWindow.conn
                );

                insertedID = Convert.ToInt32(cmd.ExecuteScalar());
            }

            catch
            {
                MessageBox.Show("This email address is already exist in our system.", "ERROR");
            }

            if (insertedID > 0) // ha ki tudott osztani az új felhasználónak új ID-t
            {
                try
                {
                    var cmd2 = new SqlCommand(
                        "INSERT INTO Profile (ForeignUserID, UserName, BirthDate, Gender, ModifiedDate) VALUES " +
                        $"('{insertedID}', '{username}', '{birthdate}', '{gender}', GETDATE()); SELECT SCOPE_IDENTITY();", 
                        MainWindow.conn
                    );

                    profileInsertedID = Convert.ToInt32(cmd2.ExecuteScalar());
                }

                catch
                {
                    MessageBox.Show("Profile data is not saved.", "ERROR");
                }

                if (profileInsertedID > 0)
                {
                    string query = $"SELECT UserID, Email, UserName, BirthDate, Gender FROM TuskeUser LEFT JOIN Profile ON TuskeUser.UserID = Profile.ForeignUserID WHERE UserID = {insertedID}";
                    var r = new SqlCommand(query, MainWindow.conn).ExecuteReader();

                    if (r.Read())
                    {
                        UserSession.CreateUser(Convert.ToInt32(r[0]), Convert.ToString(r[1]), Convert.ToString(r[2]), Convert.ToDateTime(r[3]), Convert.ToString(r[4]));
                        MainWindow.ChangePageSource("Pages/Start.xaml");
                    }
                } 
            }
            MainWindow.conn.Close();
        }
    }
}
