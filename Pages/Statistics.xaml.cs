﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Data.SqlClient;

namespace Tuske
{
    public partial class Statistics : Page
    {
        public static int NumberOfGames { get; set; }
        public static int NumberOfWonGames { get; set; }
        public static string FastesRound { get; set; }
        public static string BestRound { get; set; }
        public static double AverageStep { get; set; }
        public static string AverageTime { get; set; }
        
        public Statistics()
        {
            MainWindow.conn.Open();

            NumberOfGames = GetNumberOfGames();
            NumberOfWonGames = GetNumberOfWonGames();
            FastesRound = GetFastestRound();
            BestRound = GetBestRound();
            AverageStep = GetAverageStep();
            AverageTime = GetAverageTime();

            MainWindow.conn.Close();
            
            InitializeComponent();

            this.DataContext = this;
        }

        private int GetNumberOfGames()
        {
            string query = $"SELECT COUNT(GameID) FROM Game WHERE ForeignUserID = {UserSession.user.Id}";
            var r = new SqlCommand(query, MainWindow.conn).ExecuteScalar();
            return Convert.ToInt32(r);
        }

        private int GetNumberOfWonGames()
        {
            string query = $"SELECT COUNT(GameID) FROM Game WHERE ForeignUserID = {UserSession.user.Id} AND Result = 'Win'";
            var r = new SqlCommand(query, MainWindow.conn).ExecuteScalar();
            return Convert.ToInt32(r);
        }

        private string GetFastestRound()
        {
            string query = $"SELECT MIN(Time) FROM Game WHERE ForeignUserID = {UserSession.user.Id} AND Result = 'Win'";
            var r = new SqlCommand(query, MainWindow.conn).ExecuteScalar();
            string fastestRound = Convert.ToString(r);

            if (fastestRound == "") return "-";
            return Config.CustomTimeFormat(fastestRound);
        }

        private string GetBestRound()
        {
            string query = $"SELECT MIN(Steps) FROM Game WHERE ForeignUserID = {UserSession.user.Id} AND Result = 'Win'";
            var r = new SqlCommand(query, MainWindow.conn).ExecuteScalar();
           
            if (r.Equals(DBNull.Value)) return "0";
            if (Convert.ToInt32(r) == 1) return $"{r} step";
            return $"{r} steps";
           
        }

        private double GetAverageStep()
        {
            string query = $"SELECT ROUND(AVG(CAST(Steps AS FLOAT)), 2) FROM Game WHERE ForeignUserID = {UserSession.user.Id}";
            var r = new SqlCommand(query, MainWindow.conn).ExecuteScalar();

            if (r.Equals(DBNull.Value)) return 0;
            return Convert.ToDouble(r);
        }

        private string GetAverageTime()
        {
            string query = $"SELECT cast(cast(avg(cast(CAST(Time as datetime) as float)) as datetime) as time(7)) FROM Game WHERE ForeignUserID = {UserSession.user.Id}";
            var r = new SqlCommand(query, MainWindow.conn).ExecuteScalar();

            string averageTime = Convert.ToString(r);

            if (averageTime == "") return "-";
            return Config.CustomTimeFormat(averageTime);
        }

        private void BtnBackClick(object sender, RoutedEventArgs e)
        {
            MainWindow.ChangePageSource("Pages/Start.xaml");
        }
    }
}
