﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Text;


namespace Tuske
{

    public struct Color
    {
        public string name;
        public string hexa;
    }

    class Config
    {
        public static List<Color> codePegColors = new List<Color>() {
            new Color() { name = "red", hexa = "#EF5350" },
            new Color() { name = "orange", hexa = "#FFA726" },
            new Color() { name = "yellow", hexa = "#FFEE58" },
            new Color() { name = "green", hexa = "#4CAF50" },
            new Color() { name = "blue", hexa = "#4FC3F7" },
            new Color() { name = "purple", hexa = "#9575CD" }
        };

        public static string colorBlack = "#000000";

        public static string colorWhite = "#FFFFFF";

        public static string colorGrey = "#78909C";

        public static Color GetCodePegColorByIndex(int index)
        {
            return codePegColors[index];
        }

        public static int GetCodePegColorByHexa(string hexa)
        {
            int index = -1;
            int i = 0;

            while (i < codePegColors.Count && codePegColors[i].hexa != hexa)
            {
                i++;
            }

            if (i < codePegColors.Count)
            {
                index = i;
            }
            return index;
        }

        public static string CustomTimeFormat(string datetime)
        {
            // datetime = "12:25:36.8544879"
            string[] hourAndMin = datetime.Split(':');
            string[] secAndMilli = hourAndMin[2].Split('.');

            int hour = int.Parse(hourAndMin[0]);
            int min = int.Parse(hourAndMin[1]);
            int sec = int.Parse(secAndMilli[0]);
            int millisec = int.Parse(secAndMilli[1].Substring(0, 3));
            return String.Format("{0:mm}:{0:ss}.{0:FF}", new DateTime(1960, 1, 1, hour, min, sec, millisec));
        }

        // MD5 jelszó titkosítás: https://stackoverflow.com/questions/11454004/calculate-a-md5-hash-from-a-string
        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        // email validálás: https://docs.microsoft.com/en-us/dotnet/standard/base-types/how-to-verify-that-strings-are-in-valid-email-format
        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    string domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException e)
            {
                return false;
            }
            catch (ArgumentException e)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email,
                    @"^[^@\s]+@[^@\s]+\.[^@\s]+$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
    }
}
