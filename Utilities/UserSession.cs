﻿using System;

namespace Tuske
{
    public struct UserData
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Gender { get; set; }

        public UserData(int id, string email, string username, DateTime birthdate, string gender)
        {
            Id = id;
            Email = email;
            UserName = username;
            BirthDate = birthdate;
            Gender = gender;
        }
    }
    class UserSession
    {
        public static UserData user;

        public static void CreateUser(int id, string email, string username, DateTime birthdate, string gender)
        {
            user = new UserData(id, email, username, birthdate, gender);
        }
    }
}
