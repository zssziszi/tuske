﻿using System.Windows.Controls;

namespace Tuske
{
    public partial class HiddenCode : UserControl
    {
        public string Color0 { get; set; }
        public string Color1 { get; set; }
        public string Color2 { get; set; }
        public string Color3 { get; set; }
        public bool Clickable { get; set; }

        public HiddenCode()
        {
            #if DEBUG
                Color0 = Game.hiddenCode[0].hexa;
                Color1 = Game.hiddenCode[1].hexa;
                Color2 = Game.hiddenCode[2].hexa;
                Color3 = Game.hiddenCode[3].hexa;
            #else 
                Color0 = Config.colorGrey;
                Color1 = Config.colorGrey;
                Color2 = Config.colorGrey;
                Color3 = Config.colorGrey;         
            #endif

            if (Game.IsArchive)
            {
                Color0 = Game.hiddenCode[0].hexa;
                Color1 = Game.hiddenCode[1].hexa;
                Color2 = Game.hiddenCode[2].hexa;
                Color3 = Game.hiddenCode[3].hexa;
            }

            Clickable = false;

            InitializeComponent();
            this.DataContext = this;
        }
    }
}
