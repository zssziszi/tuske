﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;


namespace Tuske
{
   
    public partial class Row : UserControl
    {
        public static readonly DependencyProperty DataIndexProperty = DependencyProperty.Register("DataIndex", typeof(int), typeof(Row),
            new FrameworkPropertyMetadata(-1, new PropertyChangedCallback(OnDataIndexChanged)));

        public int DataIndex
        {
            get { return (int)GetValue(DataIndexProperty); }
            set { SetValue(DataIndexProperty, value); }
        }

 
        public string Color0 { get; set; }
        public string Color1 { get; set; }
        public string Color2 { get; set; }
        public string Color3 { get; set; }
        public bool Clickable { get; set; }
        
        public string ButtonVisibility { get; set; }

        public string[] KeyPegColors { get; set; }


        public Row()
        {
            //CodePeg
            Color0 = Config.colorGrey;
            Color1 = Config.colorGrey;
            Color2 = Config.colorGrey;
            Color3 = Config.colorGrey;
            Clickable = false;
            ButtonVisibility ="Hidden";

            //KeyPeg
            KeyPegColors = new string[] { Config.colorGrey , Config.colorGrey , Config.colorGrey , Config.colorGrey };

            InitializeComponent();

            this.DataContext = this;
        }

        private static void OnDataIndexChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            Row control = (Row)obj; //control-nak nevezi az aktuális Row-t
            RoutedPropertyChangedEventArgs<int> e = new RoutedPropertyChangedEventArgs<int>((int)args.OldValue, (int)args.NewValue, DataIndexChangedEvent);
            control.OnDataIndexChanged(e);
            
            if ((int)args.NewValue > -1)
            {
                if ((int)args.NewValue < Game.gameData.Count)
                {
                    GuessAndFeedback rowData = Game.gameData[(int)args.NewValue];   
                    control.ChangeProperties(rowData);
                }

                else if ((int)args.NewValue == Game.gameData.Count)
                {
                    GuessAndFeedback rowData = new GuessAndFeedback()
                    {
                        guess = new int[] { -1, -1, -1, -1 }, //-1-es értéket adtunk a szürke alapszínnek
                        correctColor = 0,
                        correctPosition = 0,
                        clickable = Game.IsArchive? false : true,
                    };
                    control.ChangeProperties(rowData);
                }
            }
        }

        
        private static readonly RoutedEvent DataIndexChangedEvent = EventManager.RegisterRoutedEvent("DataIndexChanged", RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<int>), typeof(Row));

       
        private event RoutedPropertyChangedEventHandler<int> ColorChanged
        {
            add { AddHandler(DataIndexChangedEvent, value); }
            remove { RemoveHandler(DataIndexChangedEvent, value); }
        }

        protected virtual void OnDataIndexChanged(RoutedPropertyChangedEventArgs<int> args)
        {
            RaiseEvent(args);
        }

        private void ChangeProperties(GuessAndFeedback rowData)
        {
            //CodePeg
            Color0 = rowData.guess[0] == -1 ? Config.colorGrey : Config.codePegColors[rowData.guess[0]].hexa;
            Color1 = rowData.guess[1] == -1 ? Config.colorGrey : Config.codePegColors[rowData.guess[1]].hexa;
            Color2 = rowData.guess[2] == -1 ? Config.colorGrey : Config.codePegColors[rowData.guess[2]].hexa;
            Color3 = rowData.guess[3] == -1 ? Config.colorGrey : Config.codePegColors[rowData.guess[3]].hexa;
            Clickable=rowData.clickable;

            //KeyPeg
            int pointer = 0;

            for (int i = pointer; i < rowData.correctPosition; i++)
            {
                KeyPegColors[i] = Config.colorBlack;
                pointer++;
            }

            for (int i = pointer; i < (rowData.correctPosition+rowData.correctColor); i++)
            {
                KeyPegColors[i] = Config.colorWhite;
                pointer++;
            }

            ButtonVisibility = rowData.clickable ? "Visible" : "Hidden";
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            int[] guess = new int[] 
            {
                Config.GetCodePegColorByHexa(CodePeg0.Color), 
                Config.GetCodePegColorByHexa(CodePeg1.Color),
                Config.GetCodePegColorByHexa(CodePeg2.Color), 
                Config.GetCodePegColorByHexa(CodePeg3.Color)
            };

            if (guess.Min() > -1 && guess.Max() < Config.codePegColors.Count)
            {
                Game.Assessment(guess);
            }
        }
    }
}
