﻿using System;
using System.Data.SqlClient;
using System.Text.Json;
using System.Windows;
using System.Windows.Controls;


namespace Tuske
{
    public partial class GameEndScreen : UserControl
    {
        public static string TitleText { get; set; }
     
        public string HiddenCodeColor0 { get; set; }
        public string HiddenCodeColor1 { get; set; }
        public string HiddenCodeColor2 { get; set; }
        public string HiddenCodeColor3 { get; set; }

        public string LastGuessColor0 { get; set; }
        public string LastGuessColor1 { get; set; }
        public string LastGuessColor2 { get; set; }
        public string LastGuessColor3 { get; set; }

        public string StepsText { get; set; }

        public string TimeText { get; set; }

        public bool Clickable { get; set; }

        public GameEndScreen(Boolean Win)
        {
            Clickable = false;

            if (Win)
            {
                TitleText = "WIN";
            }
            else TitleText = "DEFEAT, TRY AGAIN";

            HiddenCodeColor0 = Game.hiddenCode[0].hexa;
            HiddenCodeColor1 = Game.hiddenCode[1].hexa;
            HiddenCodeColor2 = Game.hiddenCode[2].hexa;
            HiddenCodeColor3 = Game.hiddenCode[3].hexa;


            LastGuessColor0 = Config.GetCodePegColorByIndex(Game.gameData[Game.gameData.Count - 1].guess[0]).hexa;
            LastGuessColor1 = Config.GetCodePegColorByIndex(Game.gameData[Game.gameData.Count - 1].guess[1]).hexa;
            LastGuessColor2 = Config.GetCodePegColorByIndex(Game.gameData[Game.gameData.Count - 1].guess[2]).hexa;
            LastGuessColor3 = Config.GetCodePegColorByIndex(Game.gameData[Game.gameData.Count - 1].guess[3]).hexa;

            if (Game.gameData.Count == 1)
            {
                StepsText = $"{Game.gameData.Count} step";
            }
             
            else StepsText = $"{Game.gameData.Count} steps";

            TimeText = Game.currentTime;

            SaveData(Win);

            InitializeComponent();

            this.DataContext = this;
        }

        private void SaveData(Boolean Win)
        {

            //Games tábla adatai:
       
            MainWindow.conn.Open();
            int insertedID = 0;
            string result = Win ? "Win" : "Defeat";
            string hiddenCode = $"{Config.GetCodePegColorByHexa(Game.hiddenCode[0].hexa)}," +
                $"{Config.GetCodePegColorByHexa(Game.hiddenCode[1].hexa)}," +
                $"{Config.GetCodePegColorByHexa(Game.hiddenCode[2].hexa)}," +
                $"{Config.GetCodePegColorByHexa(Game.hiddenCode[3].hexa)}";

            string jsonSerializedGameData = JsonSerializer.Serialize(Game.gameData);

            try
            {
                TimeSpan ts = Game.stopWatch.Elapsed;
                string currentTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);

                string query = "INSERT INTO Game (ForeignUserID, Steps, Time, Result, StartDate, EndDate) VALUES " +
                   $"({UserSession.user.Id}, {Game.gameData.Count}, '{currentTime}', '{result}', '{Game.startTime}', '{DateTime.Now}');";
                var cmd = new SqlCommand($"{query} SELECT SCOPE_IDENTITY();", MainWindow.conn);

                insertedID = Convert.ToInt32(cmd.ExecuteScalar());
            }

            catch
            {
                string message = "Sorry, system can not save data of your round";
                MessageBox.Show(message, "ERROR");
            }

            //Archives tábla adatai:

            if (insertedID > 0) //ha ki tudott osztani GameID-t
            {
                try
                {
                    new SqlCommand(
                       "INSERT INTO Archive (ForeignGameID, HiddenCode, GameData) VALUES " +
                       $"({insertedID}, '{hiddenCode}', '{jsonSerializedGameData}');", MainWindow.conn).ExecuteScalar();
                }

                catch
                {
                    MessageBox.Show("Sorry, system can not save details of your round",
                    "ERROR");
                }
            }

            MainWindow.conn.Close();
        }

        private void BtnPlayAgainClick(object sender, RoutedEventArgs e)
        {
            MainWindow.ChangePageSource("Pages/Start.xaml");
        }
    }
}
