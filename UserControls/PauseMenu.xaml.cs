﻿using System.Windows;
using System.Windows.Controls;

namespace Tuske
{
    public partial class PauseMenu : UserControl
    {
        public PauseMenu()
        {
            InitializeComponent();
        }

        private void BtnBackToPlayClick(object sender, RoutedEventArgs e)
        {
            Game.RemoveChild(this);
        }

        private void BtnQuitClick(object sender, RoutedEventArgs e)
        {
            MainWindow.ChangePageSource("Pages/Start.xaml");
        }
    }
}
