﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Tuske
{
    public partial class CodePeg : UserControl
    {
        public static readonly DependencyProperty ColorProperty = DependencyProperty.Register("Color", typeof(string), typeof(CodePeg), 
            new FrameworkPropertyMetadata(Config.colorGrey, new PropertyChangedCallback(OnColorChanged)));

        public static readonly DependencyProperty ClickableProperty = DependencyProperty.Register("Clickable", typeof(Boolean), typeof(CodePeg),
            new FrameworkPropertyMetadata(false, new PropertyChangedCallback(OnClickableChanged)));

        public static readonly DependencyProperty BorderProperty = DependencyProperty.Register("Border", typeof(int), typeof(CodePeg),
           new FrameworkPropertyMetadata(0, new PropertyChangedCallback(OnBorderChanged)));

        public string Color
        {
            get { return (string)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        public Boolean Clickable
        {
            get { return (Boolean)GetValue(ClickableProperty); }
            set { SetValue(ClickableProperty, value); }
        }

        public int Border
        {
            get { return (int)GetValue(BorderProperty); }
            set { SetValue(BorderProperty, value); }
        }

        private int colorIndex = -1;
        public CodePeg()
        {
            InitializeComponent();
        }

        //Color
        private static void OnColorChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            CodePeg control = (CodePeg)obj;
            RoutedPropertyChangedEventArgs<string> e = new RoutedPropertyChangedEventArgs<string>((string)args.OldValue, (string)args.NewValue, ColorChangedEvent);
            control.OnColorChanged(e);
        }
        
        private static readonly RoutedEvent ColorChangedEvent = EventManager.RegisterRoutedEvent("ColorChanged", RoutingStrategy.Bubble,typeof(RoutedPropertyChangedEventHandler<string>), typeof(CodePeg));
        
        private event RoutedPropertyChangedEventHandler<string> ColorChanged{
            add { AddHandler(ColorChangedEvent, value); }
            remove { RemoveHandler(ColorChangedEvent, value); }
        }
        
        protected virtual void OnColorChanged(RoutedPropertyChangedEventArgs<string> args){
            RaiseEvent(args);
        }
        //Clickable
        private static void OnClickableChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            CodePeg control = (CodePeg)obj;
            RoutedPropertyChangedEventArgs<Boolean> e = new RoutedPropertyChangedEventArgs<Boolean>((Boolean)args.OldValue, (Boolean)args.NewValue, ClickableChangedEvent);
            control.OnClickableChanged(e);
        }

        private static readonly RoutedEvent ClickableChangedEvent = EventManager.RegisterRoutedEvent("ClickableChanged", RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<Boolean>), typeof(CodePeg));

        private event RoutedPropertyChangedEventHandler<Boolean> ClickableChanged
        {
            add { AddHandler(ClickableChangedEvent, value); }
            remove { RemoveHandler(ClickableChangedEvent, value); }
        }

        protected virtual void OnClickableChanged(RoutedPropertyChangedEventArgs<Boolean> args)
        {
            RaiseEvent(args);
        }

        //Border
        private static void OnBorderChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            CodePeg control = (CodePeg)obj;
            RoutedPropertyChangedEventArgs<int> e = new RoutedPropertyChangedEventArgs<int>((int)args.OldValue, (int)args.NewValue, BorderChangedEvent);
            control.OnBorderChanged(e);
        }

        private static readonly RoutedEvent BorderChangedEvent = EventManager.RegisterRoutedEvent("BorderChanged", RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<int>), typeof(CodePeg));

        private event RoutedPropertyChangedEventHandler<int> BorderChanged
        {
            add { AddHandler(BorderChangedEvent, value); }
            remove { RemoveHandler(BorderChangedEvent, value); }
        }

        protected virtual void OnBorderChanged(RoutedPropertyChangedEventArgs<int> args)
        {
            RaiseEvent(args);
        }

        private void BtnInCodePegClick(object sender, RoutedEventArgs e)
        {
            if (Clickable)
            {
                ChangeColor();
            }
        }

        private void ChangeColor()
        {
            colorIndex++;
            Color = Config.codePegColors[colorIndex].hexa;
            if (colorIndex == (Config.codePegColors.Count - 1)) colorIndex = -1;
        }

        private void BtnInCodePegGotFocus(object sender, RoutedEventArgs e)
        {
            if (Clickable)
            {
                Border = 1;
            }        
        }

        private void BtnInCodePegLostFocus(object sender, RoutedEventArgs e)
        {
            if (Clickable)
            {
                Border = 0;
            }
        }
    }
}
