﻿using System.Windows;
using System.Windows.Controls;


namespace Tuske
{
    public partial class KeyPeg : UserControl
    {
        public static readonly DependencyProperty ColorProperty = DependencyProperty.Register("Color", typeof(string), typeof(KeyPeg),
            new FrameworkPropertyMetadata(Config.colorGrey, new PropertyChangedCallback(OnColorChanged)));

        public string Color
        {
            get { return (string)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }
        public KeyPeg()
        {
            InitializeComponent();
        }

        private static void OnColorChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            KeyPeg control = (KeyPeg)obj;
            RoutedPropertyChangedEventArgs<string> e = new RoutedPropertyChangedEventArgs<string>((string)args.OldValue, (string)args.NewValue, ColorChangedEvent);
            control.OnColorChanged(e);
        }

        private static readonly RoutedEvent ColorChangedEvent = EventManager.RegisterRoutedEvent("ColorChanged", RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<string>), typeof(KeyPeg));

        private event RoutedPropertyChangedEventHandler<string> ColorChanged
        {
            add { AddHandler(ColorChangedEvent, value); }
            remove { RemoveHandler(ColorChangedEvent, value); }
        }

        protected virtual void OnColorChanged(RoutedPropertyChangedEventArgs<string> args)
        {
            RaiseEvent(args);
        }
    }
}
