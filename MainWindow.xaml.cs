﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows.Navigation;

namespace Tuske
{
   
    public partial class MainWindow : NavigationWindow
    {
        public static SqlConnection conn;
        private static string dbFileLocation;
        public static MainWindow Instance { get; private set; }
        public MainWindow()
        {

            #if DEBUG
                dbFileLocation = AppDomain.CurrentDomain.BaseDirectory.Replace(@"bin\Debug\", @"AppData\tuske.mdf");
            #else
                dbFileLocation = $"{Directory.GetCurrentDirectory()}\\AppData\\Tuske.mdf";
            #endif

            conn = new SqlConnection(
               @"Server = (localdb)\MSSQLLocalDB;" +
               $"AttachDbFileName = {dbFileLocation}");

            InitializeComponent();

            Instance = this;
  
            this.Source = new Uri("Pages/Login.xaml", UriKind.Relative);
        }

        public static void ChangePageSource(string source)
        {
            Instance.Navigate(new Uri(source, UriKind.Relative));
        }
        public static void OpenGamePage(List<Color> hiddenCode, List<GuessAndFeedback> gameData, string gameTime)
        { 
            Instance.Navigate(new Game(hiddenCode, gameData, gameTime));
        }
    }
}
